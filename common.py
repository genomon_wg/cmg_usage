#!/usr/bin/env python

import math
import re


re_time = re.compile('@time\[(.*)\] time\(real,user,sys\) = (\S+)\s(\S+)\s(\S+)')


command_list = [
    'bamtofastq',
    'split ',
    'split_file ',
    'count_lines',
    'bwa_mem',
    'split_into_chrs',
    'scatter_sam',
    'cat',
    'bamsort',
    'bammarkduplicates',
    'remove_chr_dir',
    'samtools cat',
    'bai_merge',
    'samtools index',
    'md5sum',
    'EBFilter',
    'cp',
]


def normalize_command_name(str):
    for command in command_list:
        if str.startswith(command):
            return command.rstrip()
    return str


def get_elapsed_time(file_name):
    time_list = []
    with open(file_name) as f:
        for line in f:
            m = re_time.match(line)
            if m:
                command = normalize_command_name(m.group(1))
                etime = float(m.group(2))
                time_list.append([command, etime])
    return time_list


def print_conversion(command, t0, t, factor, task_factor=None):
    if task_factor:
        print('{:<23}: {:8.2f} x <{:.2f}> x ceil(<{:.2f}>) ==> {:8.2f}'.format(
              command, t0, factor, task_factor, t))
    else:
        print('{:<23}: {:8.2f} x <{:.2f}> ==> {:8.2f}'.format(
              command, t0, factor, t))


def print_total_time(t):
    print('total  {:8.2f}'.format(t))


def convert_time(time_list, conv_factor, t0_total):
    print('')
    t_total = 0
    t0_command_total = 0
    task_factor = conv_factor.get('*task*', None)
    for command, t0  in time_list:
        t0_command_total += t0
        factor = conv_factor[command]
        t = t0 * factor
        if task_factor: t *= math.ceil(task_factor)
        print_conversion(command, t0, t, factor, task_factor)
        t_total += t
    rest0 = t0_total - t0_command_total
    factor = conv_factor['*rest*']
    rest = rest0 * factor
    if task_factor: rest *= math.ceil(task_factor)
    print_conversion('*rest*', rest0, rest, factor, task_factor)
    t_total += rest

    print('')
    print_total_time(t_total)

    return t_total


def print_job_title(job_name):
    print('')
    print('-' * 80)
    print('==== {} ===='.format(job_name))
    print('')
