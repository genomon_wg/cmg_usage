#!/usr/bin/env python

import os
import sys

#from common import *
import vge_joblist_csv
import vge_job


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('usage: {} vge_output_directory'.format(sys.argv[0]))
        sys.exit(1)

    #print('Estimation of the target problem performance')
    #print('')

    print('vge_output: {}'.format(os.path.abspath(sys.argv[1])))
    vge_output_dir = sys.argv[1] + '/'
    input_csv = vge_output_dir + 'vge_joblist.csv'

    job_list, job_index = vge_joblist_csv.parse(input_csv)

    vge_job.sum_up(job_list, job_index, vge_output_dir)

