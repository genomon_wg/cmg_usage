# cmg_usage

GenomonPipeline実行時のVGEタスクおよびタスク内の各計測区間について、
経過時間を全使用CMG(MPIプロセス)にまたがって集計する。

## Usage

    cmg_usage.py (path_to)/vge_output

結果は標準出力に出力されます。

## 動作確認

Python 2.7.16 で動作確認しています。
