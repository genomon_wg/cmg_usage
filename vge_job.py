#!/usr/bin/env python

import re
import sys
import datetime
import itertools

from common import *


Tasks = {
#   '00': 'VGE master process',
   '1a': 'bam2fastq',
   '3a': 'fastq_splitter',
   '4a': 'bwa_align',
   '5a': 'markduplicates',
   '6a': 'mutation_call', 
   '7a': 'mutation_merge',
   '8a': 'bam_merge', 
}

def get_total_time(job_list):
    s = datetime.datetime.max
    f = datetime.datetime.min
    for job in job_list:
        s = min(s, job['start_dt'])
        f = max(f, job['finish_dt'])
    return (f - s).total_seconds()


def get_num_worker(job_list):
    n = 0
    for job in job_list:
        n = max(n, job['worker'])
    return n

def sum_up_tasks(job_list, job_index):
    task_time = {}
    #for task_no, task_name in Tasks.iteritems():
    #    task_time[task_name] = 0.0
    for task_name, task_job_index in job_index.iteritems():
        for id in itertools.chain(*task_job_index):
            #task_time[task_name] += job_list[id]['time']
            task_time[task_name] = task_time.get(task_name, 0) + job_list[id]['time']
    return task_time


def sum_up_task_detail(job_list, task_job_index, vge_output_dir):
    command_time = {}
    for id in itertools.chain(*task_job_index):
        t_total = job_list[id]['time']
        file = job_list[id]['stderr_file']
        time_list = get_elapsed_time(vge_output_dir + file)
        t_command_total = 0
        for command, t in time_list:
            t_command_total += t
            command_time[command] = command_time.get(command, 0) + t
        command_time['*rest*'] = command_time.get('*rest*', 0) + (t_total - t_command_total)
    #for command, t in command_time.iteritems():
    #    print '    ', command, t
    return command_time

def sum_up_detail(job_list, job_index, vge_output_dir):
    command_time = {}
    for task in Tasks.keys():
        name = Tasks[task]
        if not name in job_index.keys(): continue
        #print(name)
        time_list = sum_up_task_detail(job_list, job_index[name], vge_output_dir)
        for command, t in time_list.iteritems():
            key = (command, task)
            command_time[key] = t
    return command_time


def sum_up(job_list, job_index, vge_output_dir):

    print('')
    total_time = get_total_time(job_list)
    print('Elapsed time: {:.2f} sec'.format(total_time))
    n_procs = get_num_worker(job_list) + 1
    print('Number of MPI process: {}'.format(n_procs))
    total_cpu_time = total_time * n_procs
    print('CMG total elapsed time : {:.2f} sec'.format(total_cpu_time))

    task_time = sum_up_tasks(job_list, job_index)

    operation_time = 0.0

    print('')
    print('{:<20}: {:<10} {:<10}'.format('task', '  time(sec)',  '  rate(%)'))
    print('-' * 50)
    for task in sorted(Tasks.keys()):
        name = Tasks[task]
        if not name in task_time.keys(): continue
        full_name = task + '(' + name + ')'
        t = task_time[name]
        rate = t / total_cpu_time * 100
        print('{:<20}: {:10.2f} {:10.6f}'.format(full_name, t, rate))
        operation_time += t
    print('{:<20}: {:10.2f} {:10.6f}'.format('VGE master process', total_time,
                                             total_time / total_cpu_time * 100))
    operation_time += total_time

    idle_time = total_cpu_time - operation_time

    print('')
    print('{:<20}: {:10.6f} %'.format('operation rate', operation_time / total_cpu_time * 100))
    print('{:<20}: {:10.6f} %'.format('idle rate', idle_time / total_cpu_time * 100))

    command_time = sum_up_detail(job_list, job_index, vge_output_dir)
    sorted_command_list = sorted(command_time.keys(),
                                 lambda x, y: cmp(command_time[x], command_time[y]),
                                 reverse = True)
    print('')
    print('{:<30}: {:<10} {:<10} {:}'.format('command', ' time(sec)',  ' rate(%)', ' rate in task(%)'))
    print('-' * 75)

    for key in sorted_command_list:
        t = command_time[key]
        command, task = key
        rate_in_task = t / task_time[Tasks[task]]
        rate = t / total_cpu_time
        print('{:<30}: {:10.2f} {:10.6f} {:10.6f}'
               .format(command + '(' + task + ')', t, rate * 100, rate_in_task * 100))
        #print '    ', command + '(' + task + ')', t


